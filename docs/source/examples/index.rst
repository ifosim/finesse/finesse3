.. include:: ../defs.hrst
.. _examples:

Examples
========

Here you will find a selection of examples for performing various simulation tasks
using |Finesse|.

.. toctree::
    :maxdepth: 1

    01_simple_cav
    02_pdh_lock
    03_near_unstable
    04_geometrical_params
    05_modulation
    06_radiation_pressure
    angular_radiation_pressure
    07_homodyne
    08_optical_spring
    09_aligo_sensitivity
    10_shifted_beam
    cavity_eigenmodes
    lock_actions
    frequency_dependant_squeezing
    coupled_cavity_commands
    inference_on_RoC
