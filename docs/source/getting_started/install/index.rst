.. include:: ../../defs.hrst
.. _installation:


Installing Finesse
==================

The recommended way to install |Finesse| is to use the Conda package. This handles the
installation of the system and Python requirements in order to run |Finesse|, and is
available on most platforms.

Alternatively, depending on your system, there may be a pre-built package available to
install from `PyPI <https://pypi.org/project/finesse/>`__ using your favorite Python
package manager.

.. toctree::

    first
    pypi
    conda
    source/index.rst
    updating
