.. include:: /defs.hrst

======
Groups
======

.. kat:analysis:: series

    :See Also:

        :kat:analysis:`parallel`

.. kat:analysis:: parallel

    :See Also:

        :kat:analysis:`series`

.. kat:analysis:: for

    :See Also:

        :kat:analysis:`series`

	:kat:analysis:`change`
