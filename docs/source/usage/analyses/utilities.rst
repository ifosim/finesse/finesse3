.. include:: /defs.hrst

=========
Utilities
=========

.. kat:analysis:: change

.. kat:analysis:: dc_fields

.. kat:analysis:: debug

.. kat:analysis:: maximize

    :See Also:

	:kat:analysis:`minimize`

.. kat:analysis:: minimize

    :See Also:

	:kat:analysis:`maximize`

.. kat:analysis:: noise_projection

.. kat:analysis:: plot

.. kat:analysis:: print

.. kat:analysis:: print_model

.. kat:analysis:: print_model_attr

.. kat:analysis:: pseudo_lock_cavity

.. kat:analysis:: pseudo_lock_drfpmi

.. kat:analysis:: run_locks

.. kat:analysis:: save_matrix
