Mirror
~~~~~~

.. kat:element:: mirror

    Phase relationship
    ******************
    See :ref:`Mirror and beamsplitter phase relationships <beamsplitter_phase>`
    for more details on the phase relationship to ensure energy conservation
    that is used in |Finesse|.

    Parameters
    **********

    Listed below are the parameters of the mirror component. Certain parameters can be
    changed during a simulation and some cannot, which is highlighted in the `can change
    during simulation` column. These changeable parameters can be used by actions such
    as ``xaxis`` or ``change``. Those that cannot must be changed before a simulation is
    run.

    .. jupyter-execute::
        :hide-code:

        from finesse.utilities.misc import doc_element_parameter_table
        from finesse.components import Mirror
        doc_element_parameter_table(Mirror)

    .. _coordinate-systems:

    Coordinate systems
    ******************

    There are various coordinate systems involved in modelling the mirror component.
    Each mechanical and optical node has an associated coordinate system. Shown in the
    figure below are the coordinate systems for the optical ``p1`` (Port 1) and the
    mechanical mode ``mech``.

    .. image:: ../../../images/coordinates.svg
        :align: center
        :width: 80%

    The optical nodes, as for all optical nodes for any component, are in a left-handed
    coordinate system, shown in blue. The incoming and output going nodes represent the
    beam in the incident plane. For a mirror the angle of incidence is fixed to zero,
    :math:`\alpha=0`. A beamsplitter component is exactly the same as a mirror
    component, except that the angle of incidence can be non-zero. The outgoing node is
    a reflection of the incoming in the z and x coordinates.

    The mechanical node coordiante system is shown in black and is a right-handed
    coordinate system. A positive z motion is the surface normal of mirror surface going
    in the port 1 direction. Yaw and pitch are right-handed rotations around the y and x
    axes respectively.

    Signals
    *******

    Signals can be excited by injecting a signal into the relevant electrical or
    mechanical node at a component. The mirror component only has mechanical signals.

    Longitudinal motions
    ....................

    Small longitudinal oscillations can be excited directly by using the
    ``mirror.mech.z`` (whose units are meters) or by applying a force
    ``mirror.mech.F_z`` (units of Newtons). If no suspension is attached to the mirror
    then applying a force will not move the mirror.

    When the longitudinal motion is driven any carrier light reflected from the mirror
    surface will be phase modulated. This creates upper and lower signal sidebands
    around each carrier. Mathematically, the amount of sideband generated,
    :math:`a_{\pm}`, is linearly proportional to induced motion, :math:`z`. The higher
    order mode vector for the upper or lower sideband generated at the outgoing node of
    port 1 will be

    .. math::

        \hat{a}_{\pm} = -\I k_{\pm} r z^{\pm} \hat{E}_{c}^{i} \exp\left(\I\frac{f_{\mathrm{sig}}}{f_0}\phi\right)

    Where  :math:`k_{\pm}` is the sideband wavenumber, :math:`f_0` is the default
    optical frequency, :math:`\hat{E}_{c}^{i}` is a vector of carrier higher order
    modes, :math:`r` the mirror reflectivity, and :math:`\phi` the mirror tuning in
    radians. :math:`z^{\pm}` is defined as :math:`z^+ \equiv z` and :math:`z^- \equiv
    z^\ast`. Sidebands generated from reflection on the port 2 side will have a 180
    degree phase shift relative to the port 1.

    Yaw and pitch rotations
    .......................

    Small rotational oscillations can be excited by using the ``mirror.mech.yaw`` and
    ``mirror.mech.pitch`` both in units of radians. Torques can also be applied by using
    the ``mirror.mech.F_yaw`` or  ``mirror.mech.F_pitch``. If no suspension is attached
    to the mirror then applying a force will not rotate the mirror.

    When either yaw or pitch is driven any carrier light reflected from the surfaces has
    a linear phase shifted applied to its wavefront, thus tilting it.

    .. math::

        \hat{a}_{\pm} = -\I k_{\pm} r \beta^{\pm} \mathbf{K}_{y/p} \hat{E}_{c}^{i}  \exp\left(\I\frac{f_{\mathrm{sig}}}{f_0}\phi\right)

    The terms are the same as for the longitudinal equation except for :math:`\beta^\pm`
    which is the misalignment equivalent of :math:`z^\pm` for either pitch or yaw, and
    :math:`\mathbf{K}_{y/p}` which is the mode scattering matrix for pitch or yaw for
    some linearised unit radian alignment change.
