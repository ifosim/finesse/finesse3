.. include:: /defs.hrst

=========
Detectors
=========

.. kat:element:: amplitude_detector

.. kat:element:: astigd

.. kat:element:: beam_property_detector

.. kat:element:: ccd

.. kat:element:: ccdline

.. kat:element:: ccdpx

.. kat:element:: cp

.. kat:element:: fcam

.. kat:element:: field_detector

.. kat:element:: fline

.. kat:element:: fpx

.. kat:element:: gouy

.. kat:element:: knmd

.. kat:element:: mathd

.. kat:element:: mmd

.. kat:element:: motion_detector

.. kat:element:: optimal_q_detector

.. kat:element:: power_detector_dc

.. kat:element:: power_detector_demod_1

.. kat:element:: power_detector_demod_2

.. kat:element:: quantum_noise_detector

.. kat:element:: quantum_noise_detector_demod_1

.. kat:element:: quantum_noise_detector_demod_2

.. kat:element:: quantum_shot_noise_detector

.. kat:element:: quantum_shot_noise_detector_demod_1

.. kat:element:: quantum_shot_noise_detector_demod_2
