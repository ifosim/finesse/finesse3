.. include:: /defs.hrst

Bespoke GW Models
------------------

|Finesse| can be extended with additional packages to model
particular gravitational wave detectors. Two such packages are
finesse-ligo_ and finesse-virgo_. Additionally, packages exist to
combine Finite Element Analysis (FEA) of the test masses, with optical simulations
of the interferometer. One example is the test-mass-thermal-state_ package.

.. _finesse-ligo: https://finesse.docs.ligo.org/finesse-ligo/index.html
.. _LSC: https://finesse.docs.ligo.org/finesse-ligo/examples/plot_darm.html
.. _finesse-virgo: https://git.ligo.org/finesse/finesse-virgo
.. _test-mass-thermal-state: https://gitlab.com/ifosim/test-mass-thermal-state

This example uses finesse-ligo_ to obtain the transfer function
from Meters of test mass motion into Watts of optical power for the
LIGO Livingston Observatory.

.. warning::
    This example is based on the LSC_ example in the finesse-ligo_
    documentation. However, the code here is not automatically tested,
    as this would require making `finesse-ligo` a finesse dependency.

    Please see the finesse-ligo_ documentation for up-to-date information.


Importing `finesse-ligo`
************************
If it is installed, `finesse-ligo` can be imported using `import finesse_ligo`::

    import finesse
    import finesse_ligo
    import finesse.analysis.actions as fac

    from finesse_ligo.factory import aligo
    from finesse.plotting import bode
    from finesse_ligo.suspension import QUADSuspension

`finesse-virgo` works similarly. We then proceed to build the |Finesse| model. A
parameter file is distributed with `finesse-ligo` that contains the best estimate of all
parameters in the interferometers. In this case, we will load the parameter file for the
LIGO Livingston Observatory::

    finesse.init_plotting()
    factory = aligo.ALIGOFactory("llo_O4.yaml")

The `ALIGOFactory` function can accept either the name of a parameter file,
or, a path to a parameter file, stored somewhere on your
system.


Choosing what to model
***********************
It is computationally expensive to model the entire interferometer.
Usually, one wants to model some subset of the physics and some
subset of the ports. We now choose what to model using the factory
function::

    # Make a HOM model
    factory.reset()
    factory.options.QUAD_suspension_model = finesse_ligo.suspension.QUADSuspension
    llo3 = factory.make()
    llo3.modes("even", maxtem=2)

In this case, we have chosen to model an interferometer, with suspended
optics and the HG00, HG20 and HG02 optical modes.


Initialising the locks
**********************
A real interferometer has a real operating point enforced by feedback control.
It is critical to find this operating point using simulated locking actions.
In this case, the locks and gains are known and are
distributed with `finesse-ligo`::

    llo3.run(fac.Series(aligo.InitialLock(), aligo.DARM_RF_to_DC()))

Modelling the Interferometer
*******************************
We are now in a position to do some modelling. Such as plotting the
the transfer function::

    analysis = fac.FrequencyResponse(
        np.geomspace(0.1, 10e3, 300), "DARM.AC.i", "AS.DC", open_loop=True
    )

For further information, please see the documentation for the extension packages,
or see the :doc:`../getting_started/getting_help`_ page.
