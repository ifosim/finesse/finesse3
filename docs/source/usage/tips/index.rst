.. include:: /defs.hrst
.. _tips:

Tips and tricks
***************


Temporarily suppress warnings
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

|Finesse| will sometimes print internal warnings. We recommend that you look at these
and adjust your code to avoid them. However, if you know what you are doing and want 
to temporarily suppress
warning for a cleaner output in an interactive notebook cell you can use the following 
code in a cell:

.. jupyter-execute::

    import warnings

    with warnings.catch_warnings():
        warnings.simplefilter("ignore")

        # add your Finesse code here

Warnings will remain active in all other cells.

