"""This modules contains the functionality for building and running a sparse matrix
solver for computing the outputs of a simulation.

This is typically what earlier versions of FINESSE did in that two large sparse matrices
are built and solved for both the carrier and signal systems.
"""
